const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
mongoose.connect("mongodb+srv://alvin-estiva:gghu0C6AA0JwLlYA@mongodb-cluster.0zh91.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})
/*mongoose.connect("mongodb+srv://jeniezuitt:admin@cluster0.vsejk.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})*/


// Set notification for connection success or failure
// Connection to the database
let db = mongoose.connection;

// If a connection error occured, output a message in the console
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output a message in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Create a Task schema

const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// Default values are the predefined values for a field
		default: "pending"
	}
})

// Create a User schema.

const userSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// Default values are the predefined values for a field
		default: "pending"
	}
})

// Create a User model.

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create Models
// Server > Schema > Database > Collection (MongoDB)
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}, (err, result) => {
		// If a document was found and the document's name matches the information sent via the client/postman
		if(result != null && result.name == req.body.name){
			// Returns a message to the client/postman
			return res.send("Duplicate task found")
		}
		// If no document found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// - Create a POST route that will access the "/signup" route that will create a user.

// - Process a POST request at the "/signup" route using postman to register a user.

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username, password: req.body.password}, (err, result) => {
		if(req.body.username == '' || req.body.password == ''){
			return res.send(`Please input username and password`)
		}
		else if(result !== null && result.username == req.body.response){
			return res.send(`Username already used`)
		}
		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
		}
})

app.listen(port, () => console.log(`Server running at port ${port}`));
